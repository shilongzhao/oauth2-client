export * from './helloController.service';
import { HelloControllerService } from './helloController.service';
export * from './questionController.service';
import { QuestionControllerService } from './questionController.service';
export * from './userController.service';
import { UserControllerService } from './userController.service';
export const APIS = [HelloControllerService, QuestionControllerService, UserControllerService];
