import { TestBed, async, inject } from '@angular/core/testing';

import { QuestionAccessGuard } from './question-access.guard';

describe('QuestionAccessGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [QuestionAccessGuard]
    });
  });

  it('should ...', inject([QuestionAccessGuard], (guard: QuestionAccessGuard) => {
    expect(guard).toBeTruthy();
  }));
});
