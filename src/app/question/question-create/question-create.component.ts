import { Component, OnInit } from '@angular/core';
import { Question, QuestionControllerService } from 'app/api';

@Component({
  selector: 'app-question-create',
  templateUrl: './question-create.component.html',
  styleUrls: ['./question-create.component.scss']
})
export class QuestionCreateComponent implements OnInit {
  question: Question = {};
  modalOpen = false;
  response: QuestionCreateResponse = {};
  constructor(private questionService: QuestionControllerService) { }

  ngOnInit() {

  }

  save() {
    console.log("saving " + this.question.title);
    this.questionService.createUsingPOST(this.question).subscribe(
      data => {
        console.log("question created")
        this.response.msg = "Question has been succefully created"
        this.response.title = "Operation OK"
        this.modalOpen = true;
      },
      error => {
        console.log("error creating quesiton")
        this.response.msg = "Question creation has failed"
        this.response.title = "Operation KO"
      }
    );
  }

}

interface QuestionCreateResponse {
  title?: string;
  msg?: string;
  code?: string;
}