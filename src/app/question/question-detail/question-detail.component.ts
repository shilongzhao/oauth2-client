import { Component, OnInit, Input } from '@angular/core';
import { Question, QuestionControllerService } from 'app/api';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-question-detail',
  templateUrl: './question-detail.component.html',
  styleUrls: ['./question-detail.component.scss']
})
export class QuestionDetailComponent implements OnInit {
  @Input() question: Question;
  editing = false;

  constructor(public questionService: QuestionControllerService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.getQuestion();
  }

  getQuestion() {

    const id = +this.route.snapshot.paramMap.get('id');
    console.log('Getting details for ' + id);

    this.questionService.getQuestionByIdUsingGET(id).subscribe(
      data => {
        this.question = data;
        console.log("Got data " + data);
      },
      error => {}
    )
  }
  offer() {

  }

  refuseOffer() {
    
  }
}
