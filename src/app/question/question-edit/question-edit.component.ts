import { Component, OnInit } from '@angular/core';
import { Question, QuestionControllerService } from 'app/api';
import { Router, ActivatedRoute } from '@angular/router';
import { error } from 'util';

@Component({
  selector: 'app-question-edit',
  templateUrl: './question-edit.component.html',
  styleUrls: ['./question-edit.component.scss']
})
export class QuestionEditComponent implements OnInit {
  question: Question;
  editing: boolean = true;
  modalOpen: boolean = false;
  response: QuestionEditResponse = {};

  constructor(private questionService: QuestionControllerService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.getQuestion();
  }

  getQuestion() {

    const id = +this.route.snapshot.paramMap.get('id');
    console.log('Getting details for ' + id);

    this.questionService.getQuestionByIdUsingGET(id).subscribe(
      data => {
        this.question = data;
        console.log("Got data " + data);
      },
      error => {}
    )
  }

  save() {
    this.questionService.updateQuestionByIdUsingPUT(this.question.id, this.question).subscribe(
      data => {
        this.modalOpen = true;
        this.response.title = "Edit OK";
        this.response.msg = "Success"
;        console.log("Saved");
      }
      ,error => {
        this.modalOpen = true;
        this.response.title = "Edit KO";
        this.response.msg = "Failed"
        console.log("Error!");
      }
    );
    this.editing = false;
  }

}

interface QuestionEditResponse {
  title?: string;
  msg?: string;
  code?: string;
}
