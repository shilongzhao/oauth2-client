import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { QuestionListComponent } from './question-list/question-list.component';
import { QuestionDetailComponent } from './question-detail/question-detail.component';
import { QuestionRoutingModule } from 'app/question/question.routing';
import { ClarityModule } from '@clr/angular';
import { LayoutComponent } from 'app/layout/layout.component';
import { QuestionEditComponent } from './question-edit/question-edit.component';
import { QuestionCreateComponent } from './question-create/question-create.component';
import { QuestionAccessGuard } from 'app/question/question-access.guard';

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    ClarityModule,
    QuestionRoutingModule,
  ],
  declarations: [
    QuestionListComponent, 
    QuestionDetailComponent, 
    QuestionEditComponent, 
    QuestionCreateComponent
  ],
  providers: [
    QuestionAccessGuard
  ]
})

export class QuestionModule { }
