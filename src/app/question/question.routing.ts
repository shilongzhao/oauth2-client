import { Routes, RouterModule } from '@angular/router';
import { QuestionListComponent } from 'app/question/question-list/question-list.component';
import { QuestionDetailComponent } from 'app/question/question-detail/question-detail.component';
import { NgModule } from '@angular/core';
import { ClarityModule } from '@clr/angular';
import { LayoutComponent } from 'app/layout/layout.component';
import { PageNotFoundComponent } from 'app/page-not-found/page-not-found.component';
import { QuestionEditComponent } from 'app/question/question-edit/question-edit.component';
import { QuestionCreateComponent } from 'app/question/question-create/question-create.component';
import { QuestionAccessGuard } from 'app/question/question-access.guard';

const questionsRoutes: Routes = [
    {
        path: 'questions',
        component: LayoutComponent,
        //canActive: [AuthGuard],
        children: [
            {path: '', redirectTo: 'search', pathMatch: 'full'},
            {path: 'search', component: QuestionListComponent},
            {path: 'create', component: QuestionCreateComponent, canActivate:[QuestionAccessGuard]},
            {path: ':id', component: QuestionDetailComponent},
            {path: 'edit/:id', component: QuestionEditComponent, canActivate: [QuestionAccessGuard]},
            {path: '**', component: PageNotFoundComponent}
        ]
    }
    
];

@NgModule({
    imports:[
        RouterModule.forChild(questionsRoutes),
        ClarityModule
    ],
    exports: [RouterModule]
})

export class QuestionRoutingModule { }