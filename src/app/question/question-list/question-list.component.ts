import { Component, OnInit } from '@angular/core';
import { QuestionControllerService, Question } from 'app/api';
import { error } from 'util';
import { Router } from '@angular/router';
import { AuthService } from 'app/login/auth.service';

@Component({
  selector: 'app-question-list',
  templateUrl: './question-list.component.html',
  styleUrls: ['./question-list.component.scss']
})
export class QuestionListComponent implements OnInit {
  questions: Question[];
  selected: Question;
  constructor(public questionService: QuestionControllerService, 
      private router: Router, private authService: AuthService) { }

  ngOnInit() {
    
  }

  onSelect(q: Question) {
    this.selected = q;
    console.log('selected question ' + q);
  }

  search() {
    console.log('Getting all resources...');
    this.questionService.findAllUsingGET().subscribe(
      data => {
        this.questions = data;
        console.log(data);
      },
      error => {

      }
    );
  }

  goDetail() {
    console.log('Opening detail of ' + this.selected.id);
    this.router.navigate(['questions', this.selected.id]);
  }

  goEdit() {
    console.log('Editing detail of ' + this.selected.id);
    this.router.navigate(['questions/edit', this.selected.id]);
  }

  goCreate() {
    console.log('Creating new ... ');
    this.router.navigate(['questions/create']);
  }

  isUserAdmin() {
    return this.authService.isUserHavingRole('ADMIN');
  }
}
