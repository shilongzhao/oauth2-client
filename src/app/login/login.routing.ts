import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginPageComponent } from './login-page/login-page.component';
import { ClarityModule } from '@clr/angular';

const routes: Routes = [
    {path: 'login', component: LoginPageComponent}
]

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        ClarityModule
    ], 
    exports: [RouterModule]
})

export class LoginRoutingModule {}