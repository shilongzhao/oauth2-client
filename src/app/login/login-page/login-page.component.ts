import { Component, OnInit } from '@angular/core';
import { LoginService } from 'app/login/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {
  username: string;
  password: string;
  
  constructor(private loginService: LoginService, private router: Router) { }

  ngOnInit() {
  }

  testLogin() {
    this.loginService.getAccessToken(this.username, this.password).subscribe(
      data => {
        console.log(data.access_token)
        localStorage.setItem('access_token', data.access_token);
        localStorage.setItem('token_type', data.token_type);
        localStorage.setItem('refresh_token', data.refresh_token);
        localStorage.setItem('expires_in', String(data.expires_in));
        localStorage.setItem('scope', data.scope);
        localStorage.setItem('jti', data.jti);
        this.router.navigate(['']);
      },
      error => {

      }
    )
  }

}
