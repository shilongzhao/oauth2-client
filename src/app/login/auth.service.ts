import { Injectable } from '@angular/core';
import { isUndefined } from 'util';

@Injectable()
export class AuthService {

  constructor() { }

  getAccessToken(): string {
    return localStorage.getItem("access_token");
  }

  getJwtTokenPayload() {
    if (!this.isUserLoggedIn()) {
      return null;
    }
    return this.getAccessToken().split(".")[1];
  }

  getJwtTokenPayloadDecoded(): AccessTokenJwt {
    if (!this.isUserLoggedIn()) {
      return null;
    }
    var accessTokenJwt: AccessTokenJwt = JSON.parse(atob(this.getJwtTokenPayload()));
    return accessTokenJwt;
  }
  isJwtTokenExpired() {
    return false;
  }

  removeAccessToken() {
    console.log('removing access_token...');
    localStorage.removeItem('access_token');
  }

  getLoggedUsername():string {
    var accessTokenJwt: AccessTokenJwt = JSON.parse(atob(this.getJwtTokenPayload()));
    console.log(JSON.stringify(accessTokenJwt));
    return accessTokenJwt.user_name;
  }

  isUserLoggedIn(): boolean {
    return this.getAccessToken() != null;
  }

  isUserHavingRole(roleName: string) {
    if (!this.isUserLoggedIn()) {
      return false;
    }
    return this.getJwtTokenPayloadDecoded().authorities.indexOf('ROLE_'+roleName.toUpperCase()) > -1;
  }

  isUserHavingPermission(permissionName: string) {
    if (!this.isUserLoggedIn()) {
      return false;
    }
    return this.getJwtTokenPayloadDecoded().authorities.indexOf('ROLE_'+permissionName.toUpperCase()) > -1
  }
}

interface AccessTokenJwt {
  aud: string[];
  user_name: string;
  scope: string[];
  ati: string;
  exp: number;
  authorities: string[];
  jti: string;
  client_id: string;
}

