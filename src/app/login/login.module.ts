import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginPageComponent } from './login-page/login-page.component';
import { FormsModule } from '@angular/forms';
import { LoginRoutingModule } from 'app/login/login.routing';
import { AuthService } from 'app/login/auth.service';
import { LoginService } from 'app/login/login.service';
import { RegisterComponent } from './register/register.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    LoginRoutingModule
  ],
  declarations: [LoginPageComponent, RegisterComponent],
  providers: [
    AuthService, 
    LoginService
  ]
})
export class LoginModule { }
