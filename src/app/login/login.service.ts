import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams }               from '@angular/common/http';
import { Router } from '@angular/router';

interface OAuth2Response {
  access_token: string;
  token_type: string;
  refresh_token: string;
  expires_in: number;
  scope: string;
  jti: string;
}

@Injectable()
export class LoginService {
  authURI: string = 'http://localhost:8080/oauth/token'
  constructor(private router: Router, private http: HttpClient) { }

  /**
   * curl trusted-app:secret@localhost:8080/oauth/token -d "grant_type=password&username=user&password=password"
   * @param username 
   * @param password 
   */
  getAccessToken(username: string, password: string) {
    console.log('Getting token for ' + username)
    let params = new HttpParams()
      .append('username', username)
      .append('password', password)
      .append('grant_type', 'password');
    let authHeaders = new HttpHeaders()
      .append('Authorization', 'Basic ' + btoa('trusted-app:secret')) //  
      .set('Content-Type', 'application/x-www-form-urlencoded');
    console.log(authHeaders.get('Authorization'));
    console.log(authHeaders.get('Content-Type'));
    console.log(params.get('grant_type'));
    return this.http.post<OAuth2Response>(this.authURI, params, {headers: authHeaders});
  }

}
