import { Injectable } from '@angular/core';
import { HttpInterceptor } from '@angular/common/http';
import { AuthService } from 'app/login/auth.service';
import { HttpRequest } from '@angular/common/http/src/request';
import { HttpHandler } from '@angular/common/http/src/backend';
import { Observable } from 'rxjs/Observable';
import { HttpEvent } from '@angular/common/http/src/response';

@Injectable()
export class InterceptorService implements HttpInterceptor {

  constructor(public authService: AuthService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (request.url.indexOf('oauth') < 0 
      && request.url.indexOf('questions') < 0) { // intercept resource requests
      console.log("intercepted request " + request.url);

      const req = request.clone({
        headers: request.headers.append('Authorization', 'Bearer ' + this.authService.getAccessToken())
      });
      return next.handle(req);
    }
    return next.handle(request);
  }
}
