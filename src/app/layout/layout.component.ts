import { Component, OnInit } from '@angular/core';
import { AuthService } from 'app/login/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {

  constructor(private authService: AuthService, private router:Router) { }

  ngOnInit() {
  }

  isUserLoggedIn() {
    return this.authService.isUserLoggedIn();
  }

  getLoggedUsername() {
    console.log("getting user name");
    return this.authService.getLoggedUsername();
  }

  logout() {
    console.log("loggin out ... ");
    this.authService.removeAccessToken();
    this.router.navigate(['']);
  }
}
